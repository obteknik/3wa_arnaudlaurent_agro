SYMFONY			= php bin/console
COMPOSER		= composer
ARTEFACTS		= var/artefacts

## 
## Bases
## -----
## 

run: ## Run server
run:
	php -S 127.0.0.1:8000 -t public

update: ## Run update
update:
	$(COMPOSER) update

.PHONY: update run

# rules based on files
composer.lock: composer.json
	$(COMPOSER) update --lock --no-scripts --no-interaction

vendor: composer.lock
	$(COMPOSER) install

# Requierements
artefacts: var/artefacts
	@mkdir -p $(ARTEFACTS)

req-phpunit: vendor artefacts
	@if [ ! -f bin/phpunit ]; \
	then\
		echo "phpunit est absent.\nVoulez vous installer phpunit ? \no) Oui\nn) Non"; \
		read reponse; \
		if [ "$$reponse" = "o" ]; \
		then\
			echo "Installation de phpunit"; \
			$(COMPOSER) req --dev test-pack; \
			mkdir -p $(ARTEFACTS)/phpunit/u; \
			mkdir -p $(ARTEFACTS)/phpunit/f; \
			mkdir -p $(ARTEFACTS)/phpunit/cmd; \
		else\
			echo "Impossible de lancer phpunit"; \
			return 1; \
		fi; \
	fi

req-php_codesnifer: vendor artefacts
	@if [ ! -f vendor/bin/phpcs ]; \
	then\
		echo "php_codesniffer est absent.\nVoulez vous installer php_codesniffer ? \no) Oui\nn) Non"; \
		read reponse; \
		if [ "$$reponse" = "o" ]; \
		then\
			echo "Installation de php_codesniffer"; \
			$(COMPOSER) req --dev squizlabs/php_codesniffer; \
			mkdir -p $(ARTEFACTS)/phpcs; \
		else\
			echo "Impossible de lancer php_codesniffer"; \
			return 1; \
		fi; \
	fi

req-phpcpd: vendor artefacts
	@if [ ! -f vendor/bin/phpcpd ]; \
	then\
		echo "phpcpd est absent.\nVoulez vous installer phpcpd ? \no) Oui\nn) Non"; \
		read reponse; \
		if [ "$$reponse" = "o" ]; \
		then\
			echo "Installation de phpcpd"; \
			$(COMPOSER) req --dev sebastian/phpcpd; \
			mkdir -p $(ARTEFACTS)/phpcpd; \
		else\
			echo "Impossible de lancer phpcpd"; \
			return 1; \
		fi; \
	fi

req-phpdcd: vendor artefacts
	@if [ ! -f vendor/bin/phpdcd ]; \
	then\
		echo "phpdcd est absent.\nVoulez vous installer phpdcd ? \no) Oui\nn) Non"; \
		read reponse; \
		if [ "$$reponse" = "o" ]; \
		then\
			echo "Installation de phpdcd"; \
			$(COMPOSER) req --dev sebastian/phpdcd; \
			mkdir -p $(ARTEFACTS)/phpdcd; \
		else\
			echo "Impossible de lancer phpdcd"; \
			return 1; \
		fi; \
	fi

req-phpmetrics: vendor artefacts
	@if [[ ! -f vendor/bin/phpmetrics ]]; \
	then\
		echo "phpmetrics est absent.\nVoulez vous installer phpmetrics ? \no) Oui\nn) Non"; \
		read reponse; \
		if [ "$$reponse" = "o" ]; \
		then\
			echo "Installation de phpmetrics"; \
			$(COMPOSER) req --dev phpmetrics/phpmetrics; \
			mkdir -p $(ARTEFACTS)/phpmetrics; \
		else\
			echo "Impossible de lancer phpmetrics"; \
			return 1; \
		fi; \
	fi

.PHONY: artefacts req-phpunit req-php_codesnifer req-phpcpd req-phpdcd req-phpmetrics

## 
## Tests
## -----
## 

test: ## Run all the kind tests
test: tmvp tu tf

tcmd: ## Run command (App/Command) tests
tcmd: req-phpunit
	php bin/phpunit --coverage-html=$(ARTEFACTS)/phpunit/cmd/ --group tcmd

tmvp: ## Run MVP (Minimum Viable Product) tests
tmvp: req-phpunit
	php bin/phpunit --coverage-html=$(ARTEFACTS)/phpunit/ --group mvp

tu: ## Run unit tests
tu: req-phpunit
	php bin/phpunit --coverage-html=$(ARTEFACTS)/phpunit/u/ --exclude-group functional,mvp,cmd

tf: ## Run functional tests
tf: req-phpunit
	php bin/phpunit --coverage-html=$(ARTEFACTS)/phpunit/f/ --group functional

.PHONY: test tcmd tmvp tu tf

## 
## Quality assurance
## -----------------
## 

lint: ## Lints twig and yaml files
lint: lt ly

lt: vendor
	$(SYMFONY) lint:twig templates

ly: vendor
	$(SYMFONY) lint:yaml config

php_codesnifer: ## PHP_CodeSnifer (https://github.com/squizlabs/PHP_CodeSniffer)
php_codesnifer: req-php_codesnifer
	php vendor/bin/phpcs --standard=PSR1 src > $(ARTEFACTS)/phpcs/PSR1.txt
	php vendor/bin/phpcs --standard=PSR2 src > $(ARTEFACTS)/phpcs/PSR2.txt
	php vendor/bin/phpcs --standard=PSR12 src --ignore=/src/Kernel.php > $(ARTEFACTS)/phpcs/PSR12.txt

phpcpd: ## PHP Copy/Paste Detector (https://github.com/sebastianbergmann/phpcpd)
phpcpd: req-phpcpd
	php vendor/bin/phpcpd src > $(ARTEFACTS)/phpcpd/report.txt

phpdcd: ## PHP Dead Code Detector (https://github.com/sebastianbergmann/phpdcd)
phpdcd: req-phpdcd	
	php vendor/bin/phpdcd src > $(ARTEFACTS)/phpdcd/report.txt

phpmetrics: ## PhpMetrics (http://www.phpmetrics.org)
phpmetrics: req-phpmetrics
	php vendor/bin/phpmetrics --report-html=$(ARTEFACTS)/phpmetrics src

.PHONY: lint lt ly php_codesnifer phpcpd phpdcd phpmetrics



.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help