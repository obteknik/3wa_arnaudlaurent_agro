<?php
namespace App\Tests\Controller;

use Symfony\Component\Panther\PantherTestCase;

/**
 * @group functional
 */
class DefaultControllerTest extends PantherTestCase
{
    public function testHomepageIsUp()
    {
    	$client = static::createPantherClient();
        $crawler = $client->request('GET', '/');
        self::assertSame('Home page!', $crawler->filter('h1')->text());
    }
    
    public function testAdminpageIsUp()
    {
        $client = static::createPantherClient();
        $crawler = $client->request('GET', '/');
        self::assertSame('Admin page!', $crawler->filter('h1')->text());
    }
    
    public function testBackpageIsUp()
    {
        $client = static::createPantherClient();
        $crawler = $client->request('GET', '/');
        self::assertSame('Back page!', $crawler->filter('h1')->text());
    }

}