<?php
namespace App\Tests\Service;

use Doctrine\Common\Persistence\ObjectManager;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Service\TokenManager;
use App\Entity\Token;
use Faker\Factory;

class TokenManagerTest extends TestCase
{
    /** @var MockObject|ObjectManager */
    private $entityManager;
    
    /** @var UserPasswordEncoderInterface|ObjectManager */
    private $passwordEncoder;
    
    /** @var UserManager */
    private $manager;
    
    private $faker;
    
    protected function setUp()
    {
        $this->entityManager = $this->createMock(ObjectManager::class);
        $this->passwordEncoder = $this->createMock(UserPasswordEncoderInterface::class);
        $this->passwordEncoder->method('encodePassword')->willReturn('$2y$13$vNH4hSaJX08nnabbLZtxLunJn/3lnY1jZwy34sI82J.l74UBryl7m');
        $this->manager = new UserManager($this->entityManager,$this->passwordEncoder);
    }
    
    public function testRegister()
    {
        $this->entityManager
        ->expects(self::once())
        ->method('persist');
        
        $this->entityManager
        ->expects(self::once())
        ->method('flush');
        
        $this->faker = Factory::create();
        
        $token = new Token();
        
        $token = $this->manager->createTokenActivationForUser(User $user);
        
        self::assertInstanceOf(Token::class, $token);
    }
    
    public function testCreateFromArrayWithEmptyData()
    {
        $this->expectException(MissingOptionsException::class);
        $token = new Token();
        $this->manager->register($token);
    }
}