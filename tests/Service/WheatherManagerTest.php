<?php
namespace App\Tests\Service;

use Doctrine\Common\Persistence\ObjectManager;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use App\Service\WheatherManager;
use App\Entity\Wheather;
use Faker\Factory;

class WheatherManagerTest extends TestCase
{
    /** @var MockObject|ObjectManager */
    private $entityManager;
    
    /** @var WheatherManager */
    private $manager;
    
    private $faker;
    
    protected function setUp()
    {
        $this->entityManager = $this->createMock(ObjectManager::class);
        $this->manager = new WheatherManager($this->entityManager);
    }
    
    public function testCreateFromArray()
    {
        $this->entityManager
        ->expects(self::once())
        ->method('persist');
        
        $this->entityManager
        ->expects(self::once())
        ->method('flush');
        
        $this->faker = Factory::create();
        
        $wheather = $this->manager->createFromArray([
            'location' => ($this->faker->latitude(-90, 90).','.$this->faker->longitude(-180, 180)),
            'temperature' => $this->faker->randomFloat(1, -50, 50),
            'humidity' => $this->faker->randomFloat(1, 0, 100),
        ]);
        
        self::assertInstanceOf(Wheather::class, $wheather);
    }
    
    public function testCreateFromArrayWithEmptyData()
    {
        $this->expectException(MissingOptionsException::class);
        $this->manager->createFromArray([]);
    }
}