<?php
namespace App\Handler\Security;

use App\Entity\User;
use Doctrine\ORM\ORMException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use App\Service\UserManager;
use App\Service\TokenManager;

class RegisterHandler
{
    
    /**
     * @var LoggerInterface
     */
    private $loggerInterface;
    
    /**
     * @var UserManager
     */
    private $userManager;
    
    /**
     * @var TokenManager
     */
    private $tokenManager;
    
    /**
     * @param LoggerInterface $loggerInterface
     * @param UserManager $userManager
     * @param TokenManager $tokenManager
     */
    public function __construct(
        LoggerInterface $loggerInterface,
        UserManager $userManager,
        TokenManager $tokenManager
    )
    {
        $this->loggerInterface = $loggerInterface;
        $this->userManager = $userManager;
        $this->tokenManager = $tokenManager;
    }
    
    /**
     * @param FormInterface $form
     * @param Request $request
     * @return boolean
     */
    public function handle(FormInterface $form,Request $request)
    {
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            try {
                $user = $this->userManager->register($form->getData());
                $token = $this->tokenManager->createTokenActivationForUser($user);
            } catch (ORMException $e) {
                $this->loggerInterface->error($e->getMessage());
                $form->addError(new FormError("Erreur lors de l'insertion en base d'un user"));
                return false;
            }
            return true;
        }
        return false;
    }
}

