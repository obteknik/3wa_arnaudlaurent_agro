<?php

namespace App\Controller\Security;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

use App\Entity\User;
use App\Form\Security\RegisterType;
use App\Handler\Security\RegisterHandler;
/*
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Psr\Log\LoggerInterface;
*/

class SecurityController extends AbstractController
{
    /**
     * @Route("/security/signup", name="security_register")
     */
    public function registerAction(
        Request $request, 
        RegisterHandler $handler
    )
    {
        $form = $this->createForm(RegisterType::class);
        if($handler->handle($form, $request)){
            return $this->redirectToRoute('app_home');
        }
        
        return $this->render('security/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/security/signin", name="security_login")
     */
    public function loginAction(AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }
    
    /**
     * @Route("/security/signout", name="security_logout")
     */
    public function logoutAction(){}

    /**
     * @Route("/security/lost", name="security_lost_password")
     */
    public function lostPasswordAction(){}
}
