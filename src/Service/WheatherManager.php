<?php
namespace App\Service;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use App\Entity\Wheather;
use Faker\Factory;

class WheatherManager
{
    /**
     * @var ObjectManager
     */
    protected $entityManager;
    
    
    /**
     * @param ObjectManager $entityManager
     */
    public function __construct(ObjectManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    /**
     * @return Wheather
     */
    public function generate(): Wheather
    {
        $location = '48.8568,2.3508';
        $data = [];
        $data['location'] = $location;
        
        $curl = new \Curl\Curl();
        $curl->setBasicAuthentication('studdy_laurent', 'KrGbWt7v4N2uT');
        $curl->get('https://api.meteomatics.com/now/t_2m:C,relative_humidity_2m:p/'.$location.'/json');
        if ($curl->error) {
            echo $curl->error_code;
        }
        else {
            $curlData = json_decode($curl->response)->data;
            foreach ($curlData as $paramData){
                if($paramData->parameter == 't_2m:C'){
                    $data['temperature'] = ($paramData->coordinates[0]->dates[0]->value);
                } 
                elseif ($paramData->parameter == 'relative_humidity_2m:p') {
                    $data['humidity'] = ($paramData->coordinates[0]->dates[0]->value);
                }
            }
        }
        
        $wheather = self::createFromArray($data);
        return $wheather;
    }
    
    /**
     * @return Wheather
     */
    public function generateFake(): Wheather
    {
        $this->faker = Factory::create();
        
        $data= [
            'location' => '48.8568,2.3508',
            'temperature' => $this->faker->randomFloat(1, -50, 50),
            'humidity' => $this->faker->randomFloat(1, 0, 100),
        ];
        
        $wheather = self::createFromArray($data);
        return $wheather;
    }
    
    /**
     * @param array $data
     * @return Wheather
     */
    public function createFromArray(array $data): Wheather
    {
        if (empty($data) || (
            empty($data['location']) && 
            empty($data['temperature']) && 
            empty($data['humidity'])
        )) {
            throw new MissingOptionsException();
        }
        $wheather = new Wheather();
        
        $wheather->setLocation($data['location']);
        $wheather->setTemperature($data['temperature']);
        $wheather->setHumidity($data['humidity']);
        $wheather->setCreatedAt(new \DateTime());
        
        $this->entityManager->persist($wheather);
        $this->entityManager->flush();
        return $wheather;
    }
    
}
