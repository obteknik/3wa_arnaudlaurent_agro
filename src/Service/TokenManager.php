<?php
namespace App\Service;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use App\Entity\Token;
use Faker\Factory;

class TokenManager
{
    const TOKEN_TYPE_REGISTER = 'REG';
    const TOKEN_TYPE_LOST = 'LOST';
    const TOKEN_TYPE_API = 'API';

    /**
     * @var ObjectManager
     */
    protected $entityManager;

    /**
     * @var TokenGeneratorInterface
     */
    protected $tokenGenerator;

    /**
     * @param ObjectManager $entityManager
     * @param TokenGeneratorInterface $tokenGenerator
     */
    public function __construct(
        ObjectManager $entityManager,
        TokenGeneratorInterface $tokenGenerator
    )
    {
        $this->entityManager = $entityManager;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @param User $user
     * @return User
     */
    public function createTokenActivationForUser(User $user)
    {
        $data = [];
        $data['user']=$user;
        $data['type']=self::TOKEN_TYPE_REGISTER;
        return self::createFromArray($data);
    }
    
    /**
     * @param array $data
     * @return User
     */
    public function createFromArray(array $data): User
    {
        if (empty($data) || (
            empty($data['user']) &&
            empty($data['type']) 
        )) {
            throw new MissingOptionsException();
        }
        $token = new Token();
        $token->setToken($this->tokenGenerator->generateToken());
        $token->setUser($data['user']);
        $token->setType($data['type']);
        
        $this->entityManager->persist($token);
        
        $this->entityManager->flush();
        return $token;
    }
    
}
