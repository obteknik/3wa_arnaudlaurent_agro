<?php
namespace App\Service;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use Faker\Factory;

class UserManager
{
    /**
     * @var ObjectManager
     */
    protected $entityManager;

    /**
     * @var UserPasswordEncoderInterface
     */
    protected $passwordEncoder;

    /**
     * @param ObjectManager $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(
        ObjectManager $entityManager, 
        UserPasswordEncoderInterface $passwordEncoder
    )
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param User $user
     * @return User
     */
    public function register(User $user): User
    {
        $data= [];
        $data['firstname']=$user->getFirstname();
        $data['lastname']=$user->getLastname();
        $data['password']=$user->getPassword();
        $data['email']=$user->getEmail();
        $data['username']=$user->getUsername();
        return self::createFromArray($data);
    }
    
    /**
     * @param array $data
     * @return User
     */
    public function createFromArray(array $data): User
    {
        if (empty($data) || (
            empty($data['firstname']) &&
            empty($data['lastname']) &&
            empty($data['password']) &&
            empty($data['email']) &&
            empty($data['username'])
        )) {
            throw new MissingOptionsException();
        }
        $user = new User();
        $user->setUuid();
        
        $user->setFirstname($data['firstname']);
        $user->setLastname($data['lastname']);
        $user->setEmail($data['email']);
        $user->setUsername($data['username']);
        
        $user->setActive(false);
        $user->setRoles(['ROLE_USER']);
        $user->setCreatedAt(new \Datetime());
        $user->setUpdatedAt(new \Datetime());
        
        $password = $this->passwordEncoder->encodePassword($user, $data['password']);
        $user->setPassword($password);
        
        $this->entityManager->persist($user);
        
        $this->entityManager->flush();
        return $user;
    }
    
}
