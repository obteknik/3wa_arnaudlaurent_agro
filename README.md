# Aggro
===SF4 base===
composer create-project symfony/skeleton <name>

composer require annotations

composer require --dev maker profiler sensiolabs/security-checker reqcheck
===SF4 Test===
composer require test --dev

composer req squizlabs/php_codesniffer phpmd/phpmd pdepend/pdepend sebastian/phpcpd phpmetrics/phpmetrics --dev

===SF4 front===
composer require twig asset form symfony/validator
===SF4 BDD===
composer require doctrine
composer require --dev doctrine/doctrine-fixtures-bundle
composer require fzaninotto/faker --dev
===SF4 Security===
composer require symfony/security-bundle
===SF4 Mail===
composer require symfony/swiftmailer-bundle
===SF4 curl===
composer require curl/curl